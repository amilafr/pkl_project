<?php

namespace App\Controllers;

class Pages extends BaseController
{
	public function index()
	{
		$data = [
			'title' => 'Home | WebProgramming',
			'tes' => ['satu', 'dua']
		];
		return view('pages/home', $data);
	}

	public function about()
	{
		$data = [
			'title' => 'About me'
		];
		return view('pages/about', $data);
	}

	public function contact()
	{
		$data = [
			'title' => 'Contact',
			'alamat' => [
				[
					'tipe' => 'rumah',
					'alamat' => 'Jalan mertojoyo',
					'kota' => 'Malang'
				],
				[
					'tipe' => 'kos',
					'alamat' => 'Jalan saiko',
					'kota' => 'bandung'
				]

			]
		];
		return view('pages/contact', $data);
	}
}
