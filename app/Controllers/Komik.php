<?php

namespace App\Controllers;

use App\Models\KomikModel;

class Komik extends BaseController
{
	protected $komik_model;

	public function __construct()
	{
		$this->komik_model = new KomikModel();
	}

	public function index()
	{
		$komik = $this->komik_model->findAll();
		
		$data = [
			'title' => 'Daftar Komik',
			'komik' => $komik
		];

		//$komik_model = new \App\Models\KomikModel();
		// $komik_model = new KomikModel();



		return view('komik/index', $data);
	}
}
